#include "hal_data.h"
#include "main.h"


 timer_info_t info;
 uint32_t current_period_counts;
 volatile uint32_t led_cnt = 0;
 uint32_t ctrl_temp1=0,ctrl_temp2=0;
 extern volatile uint8_t check_error;
 extern uint32_t check_error_count;
 extern uint32_t time_blink_led;

 volatile uint8_t state_error=STATE_CHECK_NO_ERROR;

 //volatile uint8_t flag_delay=0;
 //volatile uint32_t delay_count=0;




void Timer_init();
void Timer_stop();
void delay_ms(uint32_t time_delay);
void Clear_OuputPin();



FSP_CPP_HEADER
void R_BSP_WarmStart(bsp_warm_start_event_t event);
FSP_CPP_FOOTER



/*void delay_ms(uint32_t time_delay)
{
    flag_delay=1;
    while(delay_count<time_delay*38)
    {
        ;
    }
    flag_delay=0;
    delay_count=0;

}*/


void Timer_init(void)
{


    R_GPT_Open (&g_timer1_ctrl, &g_timer1_cfg);
    R_GPT_Start (&g_timer1_ctrl);

    R_GPT_Open (&g_timer2_ctrl, &g_timer2_cfg);
    R_GPT_Start (&g_timer2_ctrl);


     (void) R_GPT_InfoGet(&g_timer2_ctrl, &info);
     current_period_counts= info.period_counts;

}

void Timer_stop(void)
{
    R_GPT_Stop(&g_timer1_ctrl);
    R_GPT_Stop(&g_timer2_ctrl);

    Clear_OuputPin();

}



void hal_entry(void)
{

    bsp_io_level_t Ctrl_Pin_Value;
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_02_PIN_06, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_01, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_11, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_12, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_04, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_03, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_02, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_01, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_00, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_15, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_14, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_13, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_02, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_01, IOPORT_CFG_PORT_DIRECTION_OUTPUT);

    R_IOPORT_PinCfg(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_00, IOPORT_CFG_PORT_DIRECTION_INPUT);

    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_02, BSP_IO_LEVEL_HIGH);

    Timer_init();



       //Watchdog timer, timeout=4s
    fsp_err_t err = FSP_SUCCESS;
    err = R_IWDT_Open(&g_wdt0_ctrl, &g_wdt0_cfg);
    assert(FSP_SUCCESS == err);

    while (1)
    {
        if(check_error==0) // Not check error
        {

/*
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_02, BSP_IO_LEVEL_HIGH);
            delay_ms(250);
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_02, BSP_IO_LEVEL_LOW);
            delay_ms(250);
*/
            //(void) R_IWDT_Refresh(&g_wdt0_ctrl);






        }

        else
        {


                    err =  R_IOPORT_PinRead(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_00, &Ctrl_Pin_Value);
                    if(FSP_SUCCESS == err)
                    {
                                    if(Ctrl_Pin_Value==BSP_IO_LEVEL_HIGH)
                                    {
                                         state_error=STATE_CHECK_NO_ERROR;
                                         (void) R_IWDT_Refresh(&g_wdt0_ctrl);
                                       if(ctrl_temp1==0)
                                       {
                                           Timer_init();
                                       }


                                       /*R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_02, BSP_IO_LEVEL_HIGH);
                                       delay_ms(250);
                                       R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_02, BSP_IO_LEVEL_LOW);
                                       delay_ms(250);*/

                                       ctrl_temp2=0;
                                       ctrl_temp1++;
                                       check_error=0;
                                       check_error_count=0;





                                    }

                                    else if(Ctrl_Pin_Value==BSP_IO_LEVEL_LOW)
                                    {
                                        state_error=STATE_CHECK_ERROR;

                                               if(ctrl_temp2==0)
                                               {

                                                   Timer_stop();

                                               }


                                               R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_02, BSP_IO_LEVEL_LOW); //turn on led until quit from error

                                               time_blink_led=0;
                                               ctrl_temp1=0;
                                               ctrl_temp2++;
                                               check_error_count=0;



                                    }



                                    if((ctrl_temp1>=1000) || (ctrl_temp2>=1000)) //Reset to not overflow
                                    {
                                        ctrl_temp1=0;
                                        ctrl_temp2=0;

                                    }







                    }



        }









    }

#if BSP_TZ_SECURE_BUILD
    /* Enter non-secure code */
    R_BSP_NonSecureEnter();
#endif
}







void R_BSP_WarmStart(bsp_warm_start_event_t event)
{
if (BSP_WARM_START_RESET == event)
{
#if BSP_FEATURE_FLASH_LP_VERSION != 0

        /* Enable reading from data flash. */
        R_FACI_LP->DFLCTL = 1U;

        /* Would normally have to wait tDSTOP(6us) for data flash recovery. Placing the enable here, before clock and
         * C runtime initialization, should negate the need for a delay since the initialization will typically take more than 6us. */
#endif
}

if (BSP_WARM_START_POST_C == event)
{
    /* C runtime environment and system clocks are setup. */

    /* Configure pins. */
    R_IOPORT_Open (&g_ioport_ctrl, g_ioport.p_cfg);
}
}

#if BSP_TZ_SECURE_BUILD

BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ();

/* Trustzone Secure Projects require at least one nonsecure callable function in order to build (Remove this if it is not required to build). */
BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ()
{

}
#endif
