/*
 * main.h
 *
 *  Created on: 9 thg 5, 2022
 *      Author: Vinh Thanh
 */

#ifndef MAIN_H_
#define MAIN_H_


#define STATE_CHECK_ERROR 0
#define STATE_CHECK_NO_ERROR 1

#define  HUNDRED_PERCENT 100
#define STM32_SWAP_RENESAS_CONSTANT 22 // ~1665/100

#define PULSE 500 //~ 700/1665 IN RENESAS

#define BASE 12 //~ 9/1665 IN RENESAS , old value is 8
#define TURN (BASE*13 + 23) // old value is 10
#define CARLIB 5
#define CARLIB2 CARLIB
#define CARLIB3 (CARLIB2 + CARLIB)
#define CARLIB4 (CARLIB3 + CARLIB)
#define CARLIB5 (CARLIB4 + CARLIB)
#define CARLIB6 (CARLIB5 + CARLIB)
#define CARLIB7 (CARLIB6 + CARLIB)
#define CARLIBB1 (CARLIB7 + CARLIB)
#define CARLIB8 (CARLIBB1 + CARLIB)
#define CARLIB9 (CARLIB8 + CARLIB)
#define CARLIB10 (CARLIB9 + CARLIB)
#define CARLIB11 (CARLIB10 + CARLIB)
#define CARLIB12 (CARLIB11 + CARLIB)
#define CARLIB13 (CARLIB12 + CARLIB)



#define LED1_TIME (BASE) //10
#define LED2_TIME (LED1_TIME + BASE) //10*2
#define LED3_TIME (LED2_TIME + BASE) //10*3
#define LED4_TIME (LED3_TIME + BASE)
#define LED5_TIME (LED4_TIME + BASE)
#define LED6_TIME (LED5_TIME + BASE)
#define LED7_TIME (LED6_TIME + BASE)
#define BREAK_TIME1 (LED7_TIME + 18)
#define LED8_TIME (BREAK_TIME1 + BASE)
#define LED9_TIME (LED8_TIME + BASE)
#define LED10_TIME (LED9_TIME + BASE)
#define LED11_TIME (LED10_TIME + BASE)
#define LED12_TIME (LED11_TIME + BASE)//135
#define LED13_TIME (LED12_TIME + BASE)
#define LED14_TIME (LED13_TIME + BASE)



void Clear_OuputPin(void);
void Toggle_User_Led(bsp_io_port_pin_t Pin);

#endif /* MAIN_H_ */
