/*
 * it.c

 *
 *  Created on: 19 thg 5, 2022
 *      Author: Vinh Thanh
 */

#include "hal_data.h"
#include "main.h"
extern  uint32_t current_period_counts;
uint32_t check_error_count=0;
volatile uint8_t check_error=0;
//extern volatile uint32_t delay_count;
//extern volatile uint8_t flag_delay;
extern volatile uint32_t led_cnt;

extern volatile uint8_t state_error;
uint32_t time_blink_led=0;
bsp_io_level_t level = BSP_IO_LEVEL_HIGH;

void Clear_OuputPin(void)
{
  //  R_IOPORT_PortWrite(&g_ioport_ctrl, BSP_IO_PORT_00, 0x0000, 0xE006);
  //  R_IOPORT_PortWrite(&g_ioport_ctrl, BSP_IO_PORT_01, 0x0000, 0x181F);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_11, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_12, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_04, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_03, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_02, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_01, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_00, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_15, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_14, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_13, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_02, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_01, BSP_IO_LEVEL_LOW);


    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_02_PIN_06, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_01, BSP_IO_LEVEL_LOW);




   // R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_00, BSP_IO_LEVEL_LOW);



}


void Toggle_User_Led(bsp_io_port_pin_t Pin)
{
           if (BSP_IO_LEVEL_LOW == level)
          {
              level = BSP_IO_LEVEL_HIGH;
          }
          else
          {
              level = BSP_IO_LEVEL_LOW;
          }
          /* Update LED on RA6M3-PK */
          R_IOPORT_PinWrite(&g_ioport_ctrl, Pin, level);

}



/* Callback function */
void Timer_Period_Callback(timer_callback_args_t *p_args)
{



    check_error_count++;



 /*   if(flag_delay==1)
    {
        delay_count++;
    } */
    if(check_error_count>=3800*5) //Delay 0.5s
    {
        check_error=1; // Enable check error
        check_error_count=0;
    }

    //---------------------------------------------------------
    if(state_error==STATE_CHECK_NO_ERROR)
        {
            time_blink_led++;
            if(time_blink_led>=19000)
            {
                time_blink_led=0;
                Toggle_User_Led(BSP_IO_PORT_03_PIN_02);
            }
        }



    //-----------------------------------------------------------




    if (p_args->event == TIMER_EVENT_CYCLE_END)
    {
//        if ((0 <= led_cnt) && (led_cnt < LED1_TIME)) //LED1
//        {
//            // Set level to output pin
//            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_02_PIN_06, BSP_IO_LEVEL_HIGH);
//
//            // Set duty cycle
//             uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED1_TIME)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
//             R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
//        }

//        if ((LED1_TIME <= led_cnt) && (led_cnt < LED2_TIME))  //LED2
//        {
//            // Set level to output pin
//            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_03_PIN_01, BSP_IO_LEVEL_HIGH);
//            // Set duty cycle
//            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED2_TIME-CARLIB)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
//            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
//        }

         if ((LED2_TIME <= led_cnt) && (led_cnt < LED3_TIME))  //LED3
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_11, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED3_TIME-CARLIB2)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED3_TIME <= led_cnt) && (led_cnt < LED4_TIME))  //LED4
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_12, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED4_TIME-CARLIB3)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED4_TIME <= led_cnt) && (led_cnt < LED5_TIME))  //LED5
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_04, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED5_TIME-CARLIB4)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED5_TIME <= led_cnt) && (led_cnt < LED6_TIME))  //LED6
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_03, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED6_TIME-CARLIB5)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED6_TIME <= led_cnt) && (led_cnt < LED7_TIME))  //LED7
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_02, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED7_TIME-CARLIB6)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED7_TIME <= led_cnt) && (led_cnt < BREAK_TIME1))  //BREAK1
        {

            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+BREAK_TIME1-CARLIBB1)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED7_TIME <= led_cnt) && (led_cnt < LED8_TIME))  //LED8
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_01, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED8_TIME-CARLIB7)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED8_TIME <= led_cnt) && (led_cnt < LED9_TIME))  //LED9
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_01_PIN_00, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED9_TIME-CARLIB8)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED9_TIME <= led_cnt) && (led_cnt < LED10_TIME)) //LED10
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_15, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED10_TIME-CARLIB9)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED10_TIME <= led_cnt) && (led_cnt < LED11_TIME)) //LED11
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_14, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED11_TIME-CARLIB10)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
        else if ((LED11_TIME <= led_cnt) && (led_cnt < LED12_TIME)) //LED12
        {
            // Set level to output pin
            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_13, BSP_IO_LEVEL_HIGH);
            // Set duty cycle
            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED12_TIME-CARLIB11)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }
//        else if ((LED12_TIME <= led_cnt) && (led_cnt < LED13_TIME)) //LED13
//        {
//            // Set level to output pin
//            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_02, BSP_IO_LEVEL_HIGH);
//            // Set duty cycle
//            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED13_TIME-CARLIB12)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
//            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
//        }
//        else if ((LED13_TIME <= led_cnt) && (led_cnt < LED14_TIME)) //LED14
//        {
//            // Set level to output pin
//            R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_00_PIN_01, BSP_IO_LEVEL_HIGH);
//            // Set duty cycle
//            uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE+LED14_TIME-CARLIB13)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
//            R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
//        }

        else
        {
            // Clear output pin
            Clear_OuputPin();

        }

        led_cnt++;
        if (led_cnt > TURN)
        {
            //led_cnt = 0;
            led_cnt=LED2_TIME;

            // Set duty cycle
           uint32_t duty_cycle_counts =(uint32_t) ( ((uint64_t)current_period_counts * ((PULSE)/STM32_SWAP_RENESAS_CONSTANT))/ HUNDRED_PERCENT);
           R_GPT_DutyCycleSet(&g_timer2_ctrl, duty_cycle_counts,  GPT_IO_PIN_GTIOCA);
        }

    }

}

/* Callback function */
void Timer_PWM_Callback(timer_callback_args_t *p_args)
{
    // Clear output pin
    if(p_args->event==TIMER_EVENT_CAPTURE_A)
    {
        Clear_OuputPin();

    }

}


